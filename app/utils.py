# -*- coding: utf-8 -*-

import inspect

def get_args(argspec):
    """Parse argspec to get argspec with corresponding default value"""
    if argspec.defaults:
        zipped = zip(argspec.args[::-1], argspec.defaults[::-1])[::-1]
        return argspec.args[:len(args)-len(zipped)] + zipped
    return argspec.args

def isview(fname):
    http_verbs = ['GET', 'POST']
    if fname.split('_')[0].upper() in http_verbs:
        return True
    return False

def build_routes(f, rule_prefix='/'):
    """Build routes from function and its args list"""
    args = get_args(inspect.getargspec(f))
