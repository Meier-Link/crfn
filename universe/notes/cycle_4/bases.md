nouvelles recherches pour contrôler le mana par l'industrie
===========================================================

1er siècle, instabilités suite de la guerre. Lorsque Fafnir est mis hors
course, les fiélons commencent à mettre la loi qu'ils connaissent en place sur
Gaia, entraînant des conflits sur plusieurs années, suivi d'une grosse période
de famine.

Xénuvia arrivera à garder le contrôle sur certains fiélons, formant une armée
que Den détruira avec les Fils de l'Ombre.

Presque un siècle d'instabilité entraînera un déséquilibre des gouvernements du
cycle précédent, entraînant dans certaines contrées des guerres civiles, qui
aboutirons à une nouvelle organisation étatique, et les alliances du cycle
précédent n'aurons plus cours.

C'est à partir de là que commencera la quête d'industrialisation. Des missions
arriverons à percer les secrets du désert de Cristal, et de la mer de Cristal
qui se trouve en son centre. Des chercheurs remettrons la main sur des reliques
du second cycle, comme notamment les piles de mana. Elles se banaliserons au
cycle suivant et seront la principale source d'énergie pour la conquête des
étoiles.
