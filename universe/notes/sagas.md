Idées de saga
=============

Récits prévus pour l'univers du CRFN (certains ont déjà été commencés).

La légende de Razen
-------------------

1er Cycle.

Récit de la guerre des dieux. Ce récit sera plutôt intégré à l'Encyclopaedia
Universalis, dans l'introduction.

Les Années Noires
-----------------

Fin du 2e cycle.

Développer le cheminement qui a amené à l'explosion qui détruira le monde.

Récit des ancêtres de Lucian von Ruthven et de la fondation de la colonie dont
il est originaire.

Cette partie doit faire l'objet d'un JdR ...

Lucian von Ruthven
------------------

3e cycle (mais rédigés par Lucian au cours du 4e cycle).

### Cycle d'Obsidienne

Ses début, et comment il est devenu "quasi" immortel.

Alors jeune veilleur, Lucian von Ruthven va faire la rencontre d'une More,
Selena.

La jeune femme était alors en route sur un véhicule, artefact d'avant les années
noires, pour explorer le monde.

Malheureusement pour elle, elle est arrivé chez les nains en entraînant toute
une armée de Troll, provoquant un conflit dont les peuples nains et moraves du
Nord se seraient bien passés.

Empêtrée dans les ennuis, Selena sera sauvée par l'enthousiaste Lucian qui va
lui proposer de l'emmener dans son voyage. Ils seront également accompagné par
un nain ami de Lucian et également curieux d'explorer le reste du monde.

Ils se lanceront ensuite dans un grand voyage au cours du quel ils rencontrerons
les différentes races de Westeärd alors qu'elles en sont encore à leurs débuts.

### Cycle de Cristal

"Vrai" début de la saga de Lucian.
Ce récit débute environ un siècle après le Cycle d'Obsidienne.

Il débute avec le premier Seigneur du Chaos à menacer le Nord du Westeärd.
Lucian vient alors de rencontrer Freya, et ensemble, accompagnés de moraves,
elfes et nains, vont lutter contre les Ettins asservis par le Seigneur du Chaos
à l'aide d'un artefact de domination de sa création.

Ce récit relate ensuite la fondation de la Cité Delta par les moraves, elfes et
nains, puis du royaume de Transylvanie par Lucian et Freya.

La suite du récit concerne toute la vie du petit royaume jusqu'à la trahison par
l'Angara, la destruction du manoir de Lucian, et l'absorption de la Transylvanie
par l'Angara. Cependant, Lucian tournera sa haine contre Fafnir, car il aura vu
l'intervention du dieu maléfique dans l'enchaînement des évènements.

### Cycle de Jade

Suite à la destruction de son royaume, la "perte" de sa femme, et le besoin de
faire le ménage dans son esprit, Lucian entraîne son dernier né, Shatten, dans
un voyage qui les mènera jusqu'en Tochitoyo.

Pour Lucian et Shatten, ce voyage sera d'une part l'occasion d'explorer un
continent qui leur est encore inconnu, et de rencontrer des gens qui les
aideront dans leurs projets. Ils rencontrerons notamment les Tengus, qui pourrons
leur faire part de leur savoir, ainsi que de techniques qui permettent de
développer leurs capacités au paroxysme avec le Mana.

C'est en revenant de se périple que Lucian et Shatten fonderons l'Ordre des Fils
de l'Ombre.

Remarque : ils partirons au Tochitoyo en rejoignant l'une des premières caravanes
à faire le voyage depuis le Westeärd. À cette période, les contacts étaient
encore très peu développées entre les continents, et les caravanes peu
fréquentes. Ici, Lucian et Shatten seront acceptés par Un elfe et sa fille, avec
qui ils découvrirons les différents peuples qui vivent au Sud du Désert de
Cristal

Jotaro Shinnen
--------------

3e cycle

### Le Destin de l'Ombre

Ce premier récit introduit les différents protagonistes du récit, et le parcours
de Jotaro avant qu'il ne devienne le chef de la guilde des voleurs de Iëssa.

Il y rencontrera Den, un agent des Fils de l'Ombre qui lui filera un coup de main
alors que Jotaro se trouvait dans une situation délicate, et lui révèlera
certaines choses sur la situation du Tochitoyo et de Sora.

### La révolte de l'Ombre

Une fois installé dans ses fonctions de chef de guilde, Jotaro va s'employer à
mener une lutte souterraine contre les fiélons qui tiennent l'empire de Sora au
nom de Xénuvia, une Seigneur de l'Ombre qui se fait passer pour la déesse de
Sora.

Apotheosis
----------

Fin du 3e cycle.

Récits de la dernière guerre du Chaos, jusqu'à la défaite de Fafnir.

L'empire d'Angara
-----------------

3e et 4e cycle.

### Garah Pulgren

Histoire de la fondation de l'Angara.

### Elanor von Ruthven

"Le redressement de l'Angara" débutera peu après l'assimilation de la
Transylvanie par l'Angara. Elanor, fille de Lucian, s'invitera en Angara et se
mettra en devoir de ramener les descendants de son mari dans le droit chemin.

Il faut rappeler qu'Elanor von Ruthven avait épousé Garah Pulgren, fondateur du
royaume qui deviendra l'Angara. Leur différences de longévité fait qu'Elanor
survivra à son mari. Elle retournera alors au manoir de son père. Les évènements
relatés dans "Le redressement de l'Angara" seront donc pour elle un retour au
royaume de son défunt mari, et les empereurs d'Angara sont tous ses descendants.

### Idées

Monarchie autoritaire, l'Empire d'Angara finira par éclater au 4e cycle en
plusieurs états.

L'ordre des Ombres
------------------

3e cycle et suivants

Toute les histoires relatives à l'Ordre des Fils de l'Ombre.

