combinaison magie / industrie en parallèle des conquêtes spatiales
==================================================================

Evolution des technologies
--------------------------

Le principal problème lié au mana était de trouver un moyen de contrôler sa
puissance. Ce qui a fait le succès des piles de mana, c'est qu'elles permirent
de résoudre ce problème pour de faibles quantités de mana. Ces piles furent
utilisées dans de nombreuses applications où il fallait une quantité d'énergie
significative sous un faible volume, comme pour les premiers véhicules motorisés,
l'alimentation en énergie d'immeubles, etc. Cependant, la quantité d'énergie
contenue par une pile de mana était trop limitée pour des projets plus ambitieux,
comme la conquête de l'espace.

Étant donné les limitations technologiques de cette première période, des
solutions alternatives furent envisagées, comme les systèmes de propulsion
utilisant la combustion de liquides appelés "ergols". Ceux-ci permirent de
mettre en orbite des satellites, d'effectuer des voyages dans les environs
immédiats de Gaïa, mais ils ne pouvaient pas être industrialisés du fait de leur
trop grandes limitations.

La découverte de l'énergie nucléaire fut une étape décisive dans la maîtrise du
mana. En effet, le potentiel d'une centrale nucléaire rendait possible la
création de champs magnétiques suffisamment puissants pour contenir de grandes
quantités de mana, et ainsi de disposer d'une quantité d'énergie nettement plus
grande qu'avec les piles de mana.

Cependant, la quantité de mana nécessaire aux voyages spatiaux, ne serait-ce que
d'une planète à une autre, impliquait un champ magnétique réellement conséquent.
La mise au point de réacteur thermo-nucléaires suffisamment puissants 
nécessitait lui-même la mise au point d'une solution pour isoler le réacteur,
car il devait atteindre des températures phénoménales. De plus, les réacteurs
nucléaires de ce type devenaient réellement encombrant, et dangereux pour
l'environnement immédiat.

Ce fut donc au prix de décennies de recherches que les chercheurs parvinrent à
mettre au point des réacteurs nucléaires de dimensions raisonnables. Les progrès
réalisés dans l'exploration de l'espace permirent également de déployer ces
installations directement dans l'espace, là où ils seraient le plus utiles
(leur puissance était démesurée, comparé aux besoins sur Gaïa).

À partir de là, ce fut une évolution de plus en plus rapide vers des réacteurs
plus puissants et de plus en plus petit. Au début, seuls les missions
scientifiques étaient envisageable puis, progressivement, un usage industriel,
puis commercial commença à se développer, notamment avec la création et le
développement de colonies extra-gaïa.

Enfin, les premiers vaisseaux de taille réduite, pour un usage individuel,
commencèrent à faire leur apparition.

À la fin de ce cycle, deux problèmes persistaient. Tout d'abord, l'absence de
limite du mana semblait permettre de dépasser la vitesse de la lumière, mais les
vaisseaux ne résistaient pas à de telles performances. Enfin, les distances
d'un système stellaire à un autre étaient encore infranchissables avec les
moyens mis au point ...

Idées de scénario
-----------------

Tandem vieux cambrioleur jeune aventurier dans la région des citées libres
(ambiance proche orient).

Mission pour récupérer les biens d'un riche potentat dans l'une des villes,
doublés et humiliés par un mercenaire employé en parallèle, qui leur pique
le butin juste après qu'ils aient réussit leur mission. Le vieux cambrioleur
n'avait jamais connu l'échec, et préfère reprendre les affaires que de
terminer comme ça (honneur).

Tout à fait au début du 5e cycle, fond de troubles et conflits engendrés par
la famine qui a suivit la guerre. Période steampunk.