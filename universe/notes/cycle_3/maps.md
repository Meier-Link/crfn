Maps du monde de Gaïa
=====================

Les descriptions suivantes sont données du point de vue des mortels.

Cosmologie
----------

Au 3e cycle, l'univers est partagé comme suit :
* Mugen, le plan des dieux,
* Les côtes d'Éternités qui délimitent l'accès au Mugen,
* Le Panthéon, sommet du monde des Célestes,
* Le Collysée, où vivent les Célestes,
* L'Élysée, entrée du plan des Célestes,
* La Mer des songes.
* L'Abyss, dans les profondeurs du monde des Fiélons,
* Le Qlyphoth, où vivent les Fiélons,
* Le Styx, qui délimite l'accès au monde des Fiélons,
* Gaïa, le plan des mortels,
* Le Gouffre de l'Oubli, qui délimite l'accès à Gaïa.

Le plan des dieux, qui rassemblent le Mugen et les côtes d'Éternités, est
innacessible au commun des mortels.

Ensuite, le plan de la Mer des Songes, qui rassemble les royaumes des Fiélons
et des Célestes forme une frontière entre le plan des dieux et celui des
mortels.

L'Abyss correspond au lieu où Fafnir est retenu prisonnier, et d'où il étend
son influence sur les Fiélons.

Les continents du monde de Gaïa
-------------------------------

### Le Westeärd

TODO

### Le Tochitôyô

#### Histoire

Xenuvia s'intéressera d'abord au Sora à cause de son importance dans la région.
Mais les territoires Ghils seront les premiers remportés à sa cause d'abord
parce que les Ghils sont déjà de puissants guerriers, mais aussi parce que leur
territoire est géographiquement plus proche des royaumes des Seigneurs du Chaos
au Sud Ouest. (Du coup, c'est moins choquant que le Tochitôyô soit relativement
proche des terres équatoriales).

Le territoire des Nezumis est également très proche des terres des Seigneurs du
Chaos, mais Xenuvia cherchait avant tout un peuple fort pour obtenir des firens
aussi puissant que possible.

Lorsque Xenuvia s'instituera déesse des Junins, Den tentera de fomenter une
révolte que Xenuvia écrasera. Den entrainera les survivants en Yasei pour les
sauver de l'empire corrompu. Le patron de la guilde des voleurs de Iessa est un
descendant de la branche des empereurs de Sora qui avait participé à la révolte.

Lors de cette révolte, Den avait réussit à glisser le doute, concernant la
divinité de Xenuvia, dans la famille impériale de Sora.

Ce récit pourra faire l'objet d'une nouvelle de la Saga des Fils de l'Ombres.

#### Géographie

Le Tochitôyô est formée par la rencontre de 5 chaînes de montagne en son centre.

Vers le Nord-ouest, une chaîne permet de rapprocher le continent du Désert de
Cristal. Cette chaîne est en fait le prolongement des falaises qui marquent la
limite Sud du Désert de Cristal (sa limite Est sera une autre chaîne qui rejoint
également la chaîne Nord-ouest du Tochitôyô).

Vers le Sud-ouest se trouve une chaîne qui rejoint la pointe Sud de la vallée
des Bannis.

En direction du Sud se trouve une troisième chaîne qui rejoint le territoire des
Seigneurs du Chaos (qui se trouve au Pôle Sud).

En direction de l'Est-sud-est se trouve la quatrième chaîne, qui marque la
frontière Sud du Sora. C'est celle-ci qui est prolongée par l'Archipel des Îles
de Jade (qui suit toute les côtes du Sora).

Au Nord-est se trouve la chaîne la plus longue du Tochitoyo. Elle rejoint (sous
l'océan) l'île d'origine des Norsks, de l'autre côté de la planète.

Entre les chaînes Nord-ouest et Sud-ouest, se trouvent des royaumes de
commerçants apparentés à ceux qui vivent au Sud du Désert de Cristal.

À l'Ouest de la chaîne Sud se trouve le territoire (marécageux) des Nezumis. À
l'Est de cette chaîne se trouve le territoire des Ghils (qui aurait été le plus
riche s'ils avaient trouvé une trêve dans leurs conflits).

Le secteur Est du continent est le territoire des Junins. 

Enfin, les Arjunis vivent dans l'Archipel des Îles de Jade.