Les esprits 
===========

La plupart des dieux ont créé des esprits. Ceux-ci disposent généralement de la
magie, mais surtout de l'immortalité de leur créateur.

Comme toute les créations de ces derniers, ils ne disposent pas de libre arbitre
et sont assujettis aux traits que leur ont donné les dieux à leur création.

Parmi ces traits le dieu créateur peut lui donner un total dévouement, et
l'esprit ainsi créé obéira aveuglément à son créateur. C'est le cas par exemple
de Nidhogg.

Heimdall
--------

Créé par Bahamut, c'est le hérault de ce dernier. Considéré par les mortels
comme une sorte de dieux des voyageurs et des marchands, il est effectivement un
grand voyageur, constamment en déplacement à travers l'univers.

Lorsque Fafnir sera défait lors du conflit du premier cycle, Bahamut le chargera
de retrouver les fragments du cœur du Mana, afin de s'assurer qu'ils aient bien
disparu.

C'est comme ça que Heimdall découvrira Lucian von Ruthven.

Freya
-----

Créée par Gaïa, elle est l'aînée des sœurs gardiennes des 12 grands fleuves du
Westeard.

**Remarque :** Lucian ne le savait pas, mais les esprits ne peuvent pas enfanter.
Cependant, et au contact de Lucian, Freya va d'abord accroître sa puissance
(grâce au pendentif) de Lucian, et ensuite, le fait de s'unir à lui va lui
permettre de devenir fertile (même si elle ne le sera que très peu).

Nidhogg
-------

Le plus puissant des esprits créés par Fafnir. Il a l'apparence d'un gigantesque
serpent doté de huit queues, qu'il mani comme des bras.

En plus d'en avoir l'apparence, Nidhogg partage la froideur des serpents. Il est
à la fois calculateur et prudent, mais il a une confiance absolue en ses
capacités.

Cernobog
--------

D'une force colossale, il a l'apparence d'un géant, mais sa peau rappelle plutôt
celle d'un reptile en plus épais, lui offrant une armure naturelle. Il est vif
mais d'une intelligence limitée, faisant de lui un bon exécutant; il est
cependant un guerrier hors pair. Il sera un chef de guerre assez bon, mais ses
hommes fuiront dès qu'il montrera un signe de faiblesse, car il les contrôlait
par la terreur.

Bergelmir
---------

**TODO :** Fait parti des premiers Seigneurs du Chaos.

Les mortels 
===========

Œuvre des dieux, les mortels sont de moindre puissance que les esprits. Du fait
de limiter leur espérance de vie, les dieux peuvent en contre partie leur
accorder la capacité à enfanter.

Les dieux créent rarement des mortels à cause de leur faiblesse, mais Gaïa
fait exception, car elle les a créé pour les faire évoluer plutôt que pour
les employer à des tâches particulières, comme le font les autres dieux.

Famille de Lucian von Ruthven
-----------------------------

**Remarque :** au début, Lucian n'aura de confiance pour aucun des deux camps des
dieux, et leurs sera même hostile. Cependant, l'acte de Fafnir à l'égard de
Dracul le portera à s'opposer à Fafnir. Malgré tout, il restera prudent avec
Bahamut et son camps. Heimdall finira tout de même par suffisamment avoir sa
confiance pour pouvoir l'aider.

### Sa famille

**Sélène :** C'est la première femme de Lucian, même s'ils n'ont pas eu de
mariage officiel. Ils n'ont malheureusement pas eu le temps d'aller jusque là,
car Lucian la perdra en même temps que leur fils (Sélène était enceinte de
plusieurs mois) lors de l'explosion du dernier réacteur de mana en activité
après les années noires.

**Freya :** fille de Gaïa, elle est une des esprits gardienne des grands fleuves
de Westeärd. Elle fait partie des plus puissants esprits créés par Gaïa. En
contre partie, elle n'avait aucune indépendance vis à vis de Gaïa. Cependant,
la proximité du fragment de cœur de Mana que possèdait Annah von Ruthven,
transmis plus tard à Lucian von Ruthven, va lui permettra petit à petit de se
libérer de ses restrictions. Plus tard, elle se mariera à Lucian, et sa
relation avec lui lui permettra d'avoir des enfants, chose normalement
impossible pour des esprits. À noter cependant qu'elle n'en aura que très peu
au regard du temps dont ils aurons disposés Lucian et elle.

**Elanor von Ruthven :** première fille de Lucian et Freya, elle sera l'épouse de
Garah Pulgren. Elle sera considérée par le peuple d'Angara, l'empire fondé à
partir du royaume de son époux, comme la reine mère des empereurs. L'ensemble
de ses interventions sur l'empire d'Angara pourrait largement faire l'objet
d'un roman.

**Dracul et Falke von Rutven :** les deux jumeaux de Lucian et Freya. Falke a les
yeux bleues et les cheveux presque argentés, tandis que son frère a les cheveux
et les yeux noirs comme la nuit. Cette différence physique se retrouve
également dans leurs caractères : Falke est calme et posé, tandis que Dracul est
impulsif. C'est sans doute pour cette raison que Fafnir pourra l'influencer et
le manipuler. Cette intervention aura pour effet de plonger les deux jumeaux
dans les deux camps opposés lors de la première grande guerre du Chaos. Ils
seront également à l'origine des deux grandes lignées descendantes de Lucian
von Ruthven : les Falkea et les Draculea. À noter qu'après la seconde grande
guerre contre le Chaos, les deux familles pourrons enfin se réunifier et ne
faire qu'un. À ce moment là, ils voudrons reprendre Lucian comme roi, mais
celui-ci refusera, trop attaché à son indépendance (et se considérant comme hors
course).

**Shatten von Ruthven :** dernier fils de Lucian et Freya. Ce sera le fondateur de
l'ordre des Fils de l'Ombre, qui consistuera quasiment son unique famille. À
côté de ce projet, instigué par son père, il fondera un foyer avec une elfe
rencontrée sur la route commercial menant en Tochitôyô. Sa descendance directe
restera fortement liée à la direction de l'Ordre des Fils de l'Ombre, et sera
la garante de la direction suivie par celui-ci.

### Lignée de Falke von Ruthven

**Meier Link :** issu d'une branche secondaire, il se liera d'amitier avec Gekido,
meneur du groupe parti pour détrôner Nidhogg, puis s'attaquer à Fafnir au fin
fond du Qlyphoth, dans l'Abysse. Il fera ainsi parti des mortels les plus
puissants du 3e cycle, après Lucian.


### Lignée de Dracul von Ruthven

TODO

### Les fils de l'Ombre

Les Fils de l'Ombre n'est pas une famille au sens biologique du terme, mais un
ordre fondé par Shatten von Ruthven avec l'aide de son père.

Cet ordre a été fondé pour lutter contre toute les ingérence des créatures du
Chaos dans le monde de Gaïa, et par extension pour préserver le monde de Gaïa.
De ce fait, l'ordre a de tout temps été opposé aux Seigneurs du Chaos, comme
une sorte de veilleurs constamment sur la brèche. Den, le dernier Grand Maître
de l'Ordre mènera ses frères dans une lutte ultime contre le dernier des
Seigneurs du Chaos : Xénuvia.

L'histoire liant Den et Xénuvia est relativement complexe. À l'origine, ils
font partie de la première génération des Fils de l'Ombre. Cependant, à cet
époque, les moyens mis en œuvre pour sélectionner les membres de l'ordre
n'étaient pas encore assez rigoureux, et Xénuvia, malgré ses grandes qualités,
étaient victime de vices : elle était motivée par le désir de pouvoir et
l'espoir d'une jeunesse éternelle. Les Seigneurs du Chaos utilisèrent ses
faiblesses pour capturer la jeune femme, lui faisant miroiter ce dont elle
rêvait. Sa puissance était telle, qu'elle finit par devenir elle-même une
Seigneur du Chaos. Lors de la dernière Grande Guerre contre le Chaos, elle
survécue de justesse et tenta de reformer une armée et repartir à la conquète du
Westeärd, alors très affaiblit. Cependant, Den ne la laissa pas faire et mena
l'Ordre dans une ultime bataille contre les forces de Xénuvia. Les armées
étaient de puissance équivalente, mais ce qui fit la différence fut les
sentiments que Xénuvia éprouvait (et avait toujours éprouvés) pour Den.

C'est ensemble qu'ils quittèrent la bataille, en même temps que le monde des
mortels. Les troupes de Xénuvia rompirent alors le combat, et les quelques
fiélons qui l'avaient suivit furent assez rapidement éliminés par les troupes
de Den.

L'Ordre aurait pu disparaître à ce moment là, mais les survivants de la
bataille décidèrent de le maintenir, mais en réstreignant l'activité militaire.
Ils rendirent publique les chroniques qu'ils possédaient du 3è cycle et
voulurent ainsi sauvegarder la mémoire des évènements et s'en servir de rappel
contre les erreurs du passé.

Malheureusement, quelques hommes trouvèrent un usage détourné de ces archives,
qui révélaient des informations clés, comme le fait qu'il était possible de
devenir quasiment l'égal d'un dieu ... Mais cette histoire fait plutôt partie
des cycles suivants.

### Lignée des empereurs d'Angara

**Garah Pulgren**, le fondateur et Rashka, son père, à l'origine de l'idée d'un
empire fort pour préparer la lutte contre les forces de Fafnir, les hordes du
Chaos. Garah règnera 50 ans, jusqu'à sa mort.

**Gad**, fils de Garah, règnera 120 ans, jusqu'à sa mort. Ce fut un bon roi, mais
il négligea de libérer le trône lorsqu'il réalisa l'étendu de sa longévité.

**Sid**, fils de Gad, règnera 50 ans, jusqu'à sa mort. S'il fut un roi pas trop
mauvais, il fut par contre laxiste avec l'éducation de son fils.

**Tyr**, fils de Sid, règnera 80 ans, jusqu'à sa mort. Lorsqu'il comprit que son
père comptait "faire sauter" une génération, Tyr renversa son père et prit le
pouvoir par la force. C'est lui qui rebaptisa l'Enrashka en Angara. Il se lanca
également dans les conquètes des terres voisines, ayant de grandes ambitions
pour son empire naissant. Il fut redouté durant tout son règne, considéré par
son peuple comme un despote.

**Memnas**, fils de Tyr, règnera 30 ans, jusqu'à sa mort, sur un champ de bataille.
Il fut en effet un grand conquérant, portant l'empire de son père jusqu'à sa
dimension définitive. C'est également Memnas qui commencera à porter les
regards sur le royaume de Lucian. Conscient des liens du sang que sa lignée a
avec Lucian, il préféra user de moyens détourner pour récupérer le royaume de
Transylvanie, fomantant une révolution pour pouvoir ensuite profiter de la
situation pour transformer le royaume en comté.

Suite à cet évènement, Elanor décide de retourner à Delta pour "faire le ménage".
Lucian la laissera partir, malgré qu'il l'ait retenue pendant tout le règne de
Tyr. Elle ne prendra pas la place de Memnas sur le trône, mais prendra place à
son conseil et prendra en main l'éducation du jeune Sidon, héritier du trône.
Cette ingérence provoquera un conflit entre Elanor et Memnas, allant même jusqu'à
une guerre civile.

Malgré le fait qu'Elanor ait acquis le soutien du peuple, Memnas ne lâcha pas, et
chercha encore à s'opposer à Elanor. Elle ne parvint pas à calmer son descendant,
et celui-ci poursuivit ses guerres, jusqu'à finalement mourrir sur un champ de
bataille (TODO à développer).

**Sidon**, fils de Memnas. Il récupéra le royaume à la mort de son père, et garda
le trône 50 ans, avant de le laisser à son fils. Elanor resta en arrière plan du
pouvoir. Elle tenta de faire revenir Lucian sur le trône de Transylvanie, mais
celui-ci était déjà parti sur d'autres projets, et l'Histoire associa la fin de
son royaume à la disparition de son roi.

**Remarque :** à la période d'Apothéosis (relatant la face cachée du dernier
conflit contre le Chaos) l'héritier du trône d'Angara va se retrouver
impliqué dans le groupe de Gekido et Arwen, ceux qui vont remonter jusqu'au
repère de Fafnir, au fin fond de l'Abyss.

Personnages clé
---------------

Impliqués dans l'Histoire.

### Westeärd

**Khan**, fondateur du plus ancien empire du Westeärd. À cette époque, les trolls
sont encore très nombreux dans les maraîs à l'Est de la grande mer intérieure
(qui n'est pas encore aussi vaste que ce qu'elle sera). Khan parviendra à
rassembler les Norsks pour lutter contre eux. Après la victoire, il sera
reconnu comme empereur de tout les Norsks et fondera le Norskland.

Malheureusement, son emprire ne lui survivra pas à cause de la mentalité des
Norsks. Khan avait placé son aîné comme hériter de son royaume, mais ses frères
se rebellèrent contre ce choix et tentèrent de le renverser.

Le plus jeune mit à jour ces projets et ses deux aînés furent bannis, ainsi que
leurs familles. Le plus jeune décida de partir à son tour. Vu qu'il n'était pas
encore marié, le plus agé décida de lui accorder la moitié de ses biens.

**Enrack**, le futur chef de bande, était partisant du conflit. Il tenta de fomenter
une révolte et une guerre, avec pour objectif de renverser les 4 royaumes et de
reformer le Norskland. Ses plans furent découvert et il fut condamné à mort, mais
l'un de ses plus proches amis, Rashka, lui sauva la vie. S'estimant blessé dans
son honneur, Enrack conçu de la haine à son égard, mais le garda dans ses rangs,
car il était quand même un ami. Ce qui le poussera plus tard à rompre son amitié
avec Rashka, c'est le fait que Rashka commençait également à réfléchir à la
création d'un empire, et Enrack vit cette démarche comme un affront personnel.

### Tochitôyô

#### Jotaro et Akari Shinnen.

**TODO :** Adapter l'histoire déjà rédigée de Jotaro aux changements survenus sur
la carte du Tochitôyô.

Au début du récit, Jotaro fait une mission qui l'implique dans le conflit contre
les fiélons qui tiennent alors le Sora et pratiquement tout le Tochitôyô.

Il devra alors fuir la ville d'Iessa, qui la vu naître, et se lancer dans une
aventure au travers de l'empire du Sora. Il commencera par tenter de remonter
l'histoire d'Akari, l'assassin pour qui il éprouve des sentimens. Les découvertes
qu'il fera sur le chemin, ainsi que ses rencontres avec les yokais vont lui
rappeler une conversation qu'il a eu avec son patron peu de temps auparavant,
concernant les Ghils.

À l'origine, se trouve un peuple réparti en plusieurs clans qui se disputent un
territoire à l'extrême Sud-Est du Tochitoyo. L'origine du conflit résulte plus
de la nature belliqueuse des Ghils qu'autre chose. Cependant, le fait que
quelques tribus Ghils se soient unies à des Junins, alors que la plupart
accordent une grande importance à la pureté du sang, n'a certainement pas
facilité les choses.

Un événement plus tardif va faire empirer la situation. Issus d'une région au
Sud Ouest du Tochitoyo, les Nezumis vont se retrouver à l'étroit et chercher à
s'installer sur d'autres territoires, dont celui des Ghils. Poussé par la
nécessité, ils seront prêt à se battre pour obtenir une petite place.

Un nouveau changement interviendra lorsque Xenuvia enverra des Fiélons semer le
désordre dans la région: ils entretiendront le conflit au mieux, dans le but de
s'approprier leur puissance pour les besoins de la guerre contre le Westeard.

À l'époque de Jotaro et Akari, peu d'informations circulent sur la nature de
Xenuvia et des fiélons, et les récits les concernants sont considérés comme des
légendes. Akari et Jotaro voudront alors faire un tour en terre Ghils pour avoir
le cœur net sur me rôle des fiélons dans leur guerres.

Leurs rencontres successives avec les yokais, et les informations collectées sur
Xenuvia et les fiélons les pousserons alors à s'investire dans la lutte contre
le Chaos.

**Rappel :** c'est à la même période que l'héritier des royaumes du Nord doit fuir,
à cause d'un conflit suscité par Xenuvia. Ce sont eux qui emportent un enfant
étrange avec eux. Il s'agira d'un firen mi-Ghils mi-Fiélon.

##### Les yokais dans la saga de Jotaro.

Un renard qui aide Jotaro à quitter Iessa au début (le même que celui qui fait
son apparition au début du tome 2?).

Un chat qui fait son apparition sur la route, et qui n'accompagne jamais Jotaro
dans les villages. On peut faire des allusions discrètes au fait que Jotaro se
laisse guider (une fois ou deux maxi). Ce chat, qui est en fait celui de la
famille d'Akari, l'accompagnera jusqu'à l'ancienne résidence de sa maîtresse
(seule fois où il rentrera dans une ville). Akari reconnaitra ce chat quand elle
le croisera. Après, il restera avec Akari sur tout le chemin de cette aventure.

Les créatures qui attaquent Jotaro et Akari au retour sont des yokais corrompus
par un fiélon qui les "guides". Ils rencontrerons sans doute d'autres yokais
corrompus en territoire Ghils.

Conversation avec un kappa à la fin, qui fera référence à ces rencontres ("on
vous suit depuis que vous avez quitté Iessa).

Il y a aussi l'intervention (explicite, celle-ci) des tengus. Ils recommanderont
la prudence, pour le séjour en territoire Ghils.

Personnages principaux
----------------------

Pour les quels un récit est dédié.

Gaëlle Ombre-lame et Jan, hériter du comté de Laze.

TODO

Personnages secondaires
-----------------------

Compagnons de route de Garah Pulgren :
* Köinzell (l'aéromancien),
* Aspen Käzell (le mercenaire qui était intrigué par le destin de Garah),
* Shen Balker (le chef de la bande à laquelle Aspen apparternait),

Enrack, ennemi de Garah Pulgren, et chef de la bande qu'il dut fuire.
