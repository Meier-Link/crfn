Cycle des dieux
===============

Les dieux
---------

Sur ce cycle, la notion de calendrier n'existe pas : les dieux n'en ont pas
besoin. Ils disposent d'une conscience collective qui leur permet de savoir
tout sur tout, et de connaître toute les pensées de frères et sœurs, ou
presque.

Génèse de l'univers
-------------------

Fafnir découvrira qu'il existe effectivement une restriction à ce partage.
Il commencera alors à douter des autres dieux, et particulièrement de
Bahamut qu'il soupçonne non seulement de connaître les limites de leur
conscience collective, mais en plus de garder des connaissances pour lui.

Le fait est que Bahamut garde effectivement des connaissances pour lui, mais
cela vient de son titre de roi des dieux et des responsabilités qui lui
incombent.

Lorsque Bahamut découvrira les doutes de Fafnir, il tentera de le ramener à la
raison, mais le mal était déjà fait et Fafnir le prendra comme une attaque
personnel. Il décidera alors de monter une armée et révèlera les limites de
leur conscience collective aux autres dieux, semant le doute et la confusion
parmis eux.

La situation finira par dégénérer en guerre. Afin de désamorcer le conflit,
Bahamut révèlera son secret, à savoir qu'il etait seul détenteur de la capacité
à créer et détruire, et en fera l'usage pour détruire le cœur du mana, qui est
l'astre au centre de l'univers. Il espérait ainsi détruire la magie, mais si
les dieux en perdirent le contrôle l'astre fut seulement éparpillé dans
l'univers. De plus, la violence de l'explosion disloqua l'univers, qui se
fragmenta en plusieurs plans (la cosmologie). L'un des plans ne contenait que
le monde créé par Gaia, la déesse à l'origine de la planète du même nom. Le
hasard fit qu'elle se retrouva seule détentrice d'un secret: la majeure partie
des fragments du cœur du mana se sont retrouvés dans le système solaire qu'elle
a créé, et que sa planète en a été criblé. La principale conséquence est que le
monde qu'elle a créé a été altéré, et les espèces évoluées firent l'acquisition
du libre arbitre, et d'artéfacts de puissance constitués de mana brut.
Gaia ne révèlera son secret à personne, pas même à Bahamut, et se fit gardienne
de son sanctuaire, admirant les sociétés que commencèrent à élaborer ses
création ainsi libérées.

À l'origine, le Mana était un astre au centre de l'univers. Il cristallisait
toute la magie des dieux. Au milieu se trouvait le cœur de Mana, qui contient
les plus grands pouvoirs des dieux. Le cœur de Mana possède une âme inerte,
dont la puissance ne peut être quantifiée.

À noter que si les mortels considèrent les dieux comme tels, eux-mêmes se
considèrent comme une race simplement supérieure aux autres de par leur
immortalité et leurs puissance. Mais ils ne sont ni omniscient, ni omnipotent.

Génèse du monde de Gaïa
-----------------------

Gaïa créa tout un ama stellaire, autour de la planète qui porte son nom. Cet
ama est relativement peu dense, par rapport aux autres galaxies (du moins de
l'amas où elle se trouve).

La planète elle-même se trouve dans un système solaire binaire. Elle tourne
autour de la plus lumineuse des deux, appelée "Soleil", et à bonne distance de
la seconde, appelée "la Lanterne", car elle n'est visible que la nuit, dans la
période de l'année où Gaïa se trouve entre les deux étoiles. Sa luminosité
atteint au maximum trois à quatre fois celle de la Lune. Son nom vient
d'ailleurs du fait qu'elle permet d'y voir claire en pleine nuit. La Lanterne
peut également être visible de jour, lorsqu'elle est basse sur l'horizon. Plus
elle monte, plus sa luminosité est recouverte par celle du Soleil, jusqu'à ce
qu'elle soit totalement masquée.

L'impact du cœur de Mana aura également pour conséquent de projeter une grande
quantité de matière en orbite autour de la planète de Gaïa, créant ainsi un
anneau autour de celle-ci. Cet anneau sera appelé "la Frange" par les mortels,
à cause de son aspect, vu de la surface de la planète.

Les détails techniques sur le système solaire de Gaïa et son environnement
immédiat seront donnés sur les cycles 5 et 6.