Évolution progressive des connaissances:
========================================

Avant la première grande guerre: pur médiéval fantastique. Entre les deux
guerres: évolution progressive vers du steampunk avec d'abord la découverte de
la poudre, puis la maîtrise de la vapeur peut de temps après la seconde guerre
(ne pas oublier qu'il y a un gel des progrès tout de suite après la guerre à
cause de la famine, des révolutions, etc. Pendant quelques dizaines d'années).
Période pure steampunk pendant quelques siècles et début de
l'industrialisation, puis découverte et maîtrise de l'électricité, et tout ce
qui s'en suit sur les siècles qui suivent.

Il n'y aura que la notion d'informatique qui apparaitra sur du steampunk tardif
avec la mise au point de la mécanique basée sur la vapeur (les premiers ordis
avec écran devront sans doute attendre l'électricité pour apparaître).

Découpage officiel du temps
===========================

Le découpage en cycle est un mode retenu par les Fils de l'Ombre dans leurs
écrits. Il est également repris par la plupart des historiens à partir du 6e
cycle quand ils se penchent sur des événements anciens.

Cependant, des outils plus adaptés sont utilisés par le commun des mortels pour
les besoins au jour le jour. Cette note est là pour fournir les références
connues.

Calendriers du 2e cycle
-----------------------

Nous ne connaissons pas les premiers référentiels qui existaient sur Gaïa au
début du 2e cycle. Concernant sa fin, nous savons que chacune des grandes
puissances ont tenté d'imposer sa propre notion du temps. Par conséquent que les
3 plus importantes ont co existé.

Heureusement pour nous, ces calendriers avaient tous en commun :
* La durée d'une année (révolution de Gaïa autour du Soleil)
* La durée d'un mois (basée sur les révolutions de la Lune autour de Gaïa)
* La durée d'une semaine (découpage du mois en 4)
* Durée de la journée, et de ses subdivisions.

En revanche, chaque puissance avait sa propre "année zéro" et son propre mode de
découpage des cycles.

Aussi surprenant que celà puisse paraître, les états de l'Ouest utilisaient les
révolutions de la Lanterne autour du Soleil comme référence. Nous ignorons si
cela est liée à des projets de colonisation extra-gaïenne, ou simplement de la
mégalomanie (dominance sur l'ensemble du système solaire). Nous ignorons à quel
événement fait référence le cycle zéro de ce décompte, mais des découvertes
récentes nous ont permis de conclure qu'ils en étaient à la 3e révolution de la
Lanterne.

La puissance du Nord utilisait quant à elle un système apparenté aux dynasties,
chaque nouveau chef du gouvernement démarrant sa propre dynastie (celles-ci
étaient généralement courtes, quelques décennies maximum).
Peu de documents ont survécu aux années noires dans le Désert de Cristal. Nous
ignorons donc à quelle dynastie cette nation était arrivée, ni quand ou comment
a débuté la première.

La puissance de l'Est n'avait pas de découpage particulier, et nous ignorons à
quel événement correspond leur année zéro. Nous savons seulement qu'ils avaient
dépassé le 3e siècle.

Calendriers du 3e cycle
-----------------------

### Comput du Delta

Calendrier utilisé en Westeärd et certaines régions du Nord des Terres
Équatoriales, pour sa rigueur.

Il se base sur la fondation de la cité éponyme, qui fut l'une des premières de
son importance et qui a la double particularité d'avoir toujours été un centre
important du continent, et d'avoir constamment crû en importance au cours des 3e
et 4e cycle.

Les ans commencent et terminent au solstice d'Hiver et possède des semaines de 7
jours et 12 mois de 28 ou 29 jours (sauf au milieu de l'année, où le 6e mois
fait 30 jours). Au fil des siècles, il subit quelques corrections d'un ou deux
jours de sorte à rester constamment cohérent avec les cycles du Soleil, tout en
restant à peu près respectueux des cycles lunaires.

Le compute de Delta subira une réinitialisation à la fin de la seconde guerre du
Chaos. La datation du 2e cycle sera appelée "Ère de la Prophétie", faisant
allusion aux avertissements de Lucian sur la menace que Fafnir représentait pour
le monde de Gaïa. La seconde ère est simplement appelée "Ère moderne" et débute
à l'année de la signature du traité entre Westeärd et Tochitôyô qui marque la
paix officielle entre les deux continents.

### Compute du Sora

Calendrier de référence au Tochitôyô.

Ce calendrier débuta avec la fondation de l'Empire du Sora, et repart de zéro à
chaque changement de dynastie (il n'y en eu que 3 entre le 3e et le 4e cycle).

Aparté :
* Dynastie fondatrice du Sora
* Dynastie imposée par Xenuvia
* Dynastie de l'époux de la princesse qui renvetsera le culte à Xenuvia
* Pour le 3e changement, on peut imaginer un changement au cours du 4e cycle dû
à un empereur sans héritier.

Contrairement au compute du Delta, ce calendrier utilise la Lune comme référence
principale.
Les semaines comptent 6 jours, avec le "jour de la nouvelle Lune" qui est un jour
férié supplémentaire toute les 4 semaines.

L'année est ensuite coupé en 4 mois dont le nombre de jours varient pour coller
au cycle du Soleil. Leurs commencements coïncident avec les solstices et les
équinoxes. L'année commence le jour anniversaire de l'accession au trône du
premier empereur de la dynastie courante.
Cependant, l'usage au Tochitôyô est de parler de parler de la n-ème semaine, de
la n-ème année de la Dynastie.

À noter que si le compute de Sora sera maintenu tout au long du 4e cycle pour
les usages internes au Tochitôyô, ce sera le compute de Delta de l'Ère Moderne
qui sera utilisé pour les échanges internationaux lorsque celui-ci sera mis en
place.

4e et 5e cycles
---------------

Le compute de Delta sera conservé tout du long des 4e et 5e cycles, tandis que
le compute de Sora disparaîtra progressivement.

Toutefois un nouveau calendrier en temps universel commencera à faire son
apparition au cours du 5e cycle. Lié au développement des conquêtes spatiales,
il prendra d'abord pour base les révolutions de Gaïa autour du Soleil, puis
prendra les révolutions de la Lanterne autour du Soleil. Ce nouveau référentiel
a été imposé par les voyages spatiaux menant les gens de plus en plus loin de
Gaïa.

Le fait que le Soleil de Gaïa reste le centre de ce calendrier est dû à une
découverte de cette période selon laquelle toute les étoiles de l'amas dont il
fait parti gravitent autour d'un point situé à proximité du Soleil de Gaïa.

Ce nouveau calendrier possède un découpage en cycle (pouvant d'ailleurs porter à
confusion avec les grands cycles, qui furent adoptés plusieurs siècles
auparavant). Chaque cycle correspond à une révolution de la Lanterne autour du
Soleil. Le décompte des années est ensuite réinitialisé à chaque nouveau cycle,
permettant d'avoir des nombres plus faciles à manipuler. Un découpage en décade
(10 jours) à ensuite fait son apparition, accompagné de jours intercalés à
intervalles réguliers pour fournir une correspondance précise avec les années
solaires.

Il est à noter qu'à partir du moment où les populations hors Gaïa sont nettement
plus importantes que celles restées sur Gaïa, la référence au jours de Gaïa
n'avait plus qu'un intérêt purement physiologique (besoin de repos du corps, par
exemple). Un jour dit "universel" a été institué hors de Gaïa pour donner un
découpage précis de l'année solaire tout en restant relativement proche de
l'année dite "Gaïenne". Bien entendu, les subdivisions ont été adaptées en
conséquence.

Avec le temps, chaque colonie extra-gaïenne a développé sa propre notion de
jours adaptée aux révolutions de leur monde sur lui-même ("monde" plutôt que
"planète", car toute les colonies ne sont pas forcément sur une planète). Chaque
monde a ensuite adapté sa notion de "semaine" pour que sa durée soit le plus
proche possible de la notion de décade du calendrier universel.

La mort
=======

Lorsqu'un individu meurt dans l'univers, son âme rejoint le chemin (physique) le
plus proche vers la mer des songes.

Là, il laisse son empreinte, avant de quitter l'univers.

L'empreinte se matérialise par les souvenirs forts laissés par l'individu,
renforçant tantôt les forces des fiélons, tantôt celles de célestes, qui
représentent les deux composantes primaires du mana : le Chaos et l'Ordre.

À l'époque des pères des dieux, ils avaient vaincu la mort et colonisé la mer
des songes, profitant des routes originellement prévues pour les âmes des
défunts pour voyager dans tout l'univers.
La conséquence directe, c'est que lorsque un individu mourrai, il ne laissait
pas ou peu d'empreinte et donc pas de souvenirs.

Sous la domination des dieux, ceux-ci savaient quel risque représentaient les
abus d'usage de la mer des songes et avaient développé d'autres solutions pour
subvenir à leurs besoins en matière de déplacement.

La plupart des mortels ignoraient l'existence de la mer des songes, et encore
moins son rôle et ses avantages. Ce n'est que bien plus tard (5e~6e cycle)
qu'ils en apprendrons plus.

Du coup, l'un des problèmes de la fin du 6e cycle, c'est que des individus
retrouveront le moyen d'employer la mer des songes, pour acquérir une puissance
disproportionnée, une voie pour l'immortalité, et un moyen de se déplacer
n'importe où dans l'univers.

Après le 6e cycle et l'explosion de l'univers en plusieurs dimensions, les
chemins de la mer des songes seront définitivement restreints aux âmes des
défunts, et tout individu qui trouverait le moyen de s'y engager verrait son
corps consumé par l'anti mana.


Les rêves
---------

Remarque : ces notions sont valables pour tout les cycles.

Lorsqu'un individu rêve, une partie de son esprit voyage jusqu'à la Mer des
Songes. Là, il se bâti un monde en miniature où il évoluera. Dans cet espace, il
est seul maître consciemment ou non.

La Mer des Songes étant en réalité une dimension à part entière, ses limites
sont virtuellement infinies. En réalité, elle possède deux bordures, d'un côté
permettant d'accéder au monde des fiélons, et de l'autre au monde des célestes.
Tout le reste de la Mer des Songes est délimitée par une zone de brouillard de
plus en plus dense, cachant l'accès aux autres plans. Ce brouillard peut reculer
à l'infini, suivant les besoins des rêveurs.
Cette dimension n'est pas complètement tangible, et peu se déformer de sorte à
ne jamais laisser percevoir ses (éventuelles) limites.

Le rêveur peut construire absolument tout ce qu'il veut, mais quatre types
d'événements sortent de son contrôle. Il est à noter que les rêveurs peuvent
essayer d'influencer ces événements, avec plus ou moins de succès, mais ne sont
jamais maître de la situation.

### L'entrée de l'empreinte d'un aïeul

Comme évoqué précédemment, l'âmes des morts passent par la Mer des Songes en y
laissant une empreinte, qui n'est rien de plus que l'ensemble des souvenirs
qu'ils laissent derrière eux. Cette empreinte voyage dans la Mer des Songes au
gré de ses courants. Ceux-ci l'amène généralement à apparaître dans le monde des
rêveurs qui l'on connut de son vivant.

Il peut arriver qu'un rêveur cherche (consciemment ou non) à retrouver un aïeul
au court de ses rêves, et donc provoquer la rencontre avec son empreinte. Ceci
se traduira soit par une rencontre avec l'empreinte, soit tout simplement à
l'incarnation d'une sorte d'illusion faisant partie du rêve. La différence entre
les deux tiens seulement du fait que, dans le premier cas, le rêveur pourra
apprendre des choses qu'il ne savait pas (vu que l'empreinte contient l'ensemble
des souvenirs laissé par le mort); tandis que dans le second cas, il ne verra
qu'une ombre de ses propres souvenirs du défunt.

### Rencontre avec un autre rêveur

À l'instar de la rencontre avec l'empreinte d'un défunt, la rencontre avec un
autre rêveur peut être provoquée ou fortuite.

Dans un cas comme dans l'autre, les deux rêveurs peuvent interagir, offrant
ainsi un grand nombre de possibilités au travers ded rêves.

### Rencontres avec les Fiélons et Célestes

Ces êtres peuplent également des plans voisins de la Mer des Songes, et y font
régulièrement des incursions.

Lorsqu'ils rencontrent un rêveur, ils chercheront plus ou moins à interférer
dans son rêve, altèrant ce dernier, donnant ainsi les cauchemars peuplés de
monstruosités, ou les rêves oniriques. (todo: vérifier "onirique").

### Rencontres avec les envoyés des dieux

Les dieux ne viennent jamais en Mer des Songes, du fait des conséquences que
cela pourrait avoir sur ce plan.

Cependant, ils y envoient parfois des serviteurs, comme ça été le cas avec
Heimdall. Ce dernier a même poussé à plusieurs reprises jusqu'au monde de Gaïa,
mais c'est un cas exceptionnel, et il s'est arrangé pour le faire le moins
souvent possible à cause des perturbations que celà peut causer sur l'équilibre
des plans.

À noter qu'à partir du 4e cycle (en réalité, le processus à commencé dès le 3e),
les plans (autres que la Mer des Songes et les plans des Fiélons et Célestes)
ont fusionné. Les serviteurs des dieux (et les dieux eux-mêmes) ont de nouveau
pu voyager sans passer par la Mer des Songes.

### Conséquence des actes en Mer des Songes

Tout acte en Mer des Songes ont des impacts sur les protagonistes.

Pour les rêveurs, il ne s'agit que d'une projection de leur âme (celle-ci ne
peut pas quitter son corps à part une fois mort). L'influence des événements en
Mer des Songes sera donc purement psychologique, y compris si un intervenant
extérieur tente de provoquer la mort du rêveur. Dans ce cas particulier, les
conséquences peuvent aller très loin, mais il est bon de savoir que la "mort" du
rêveur entraîne la mort du rêve et donc le réveil du sujet sans autres
conséquences qu'une peure panique au réveil (excepté pour les sujets sensibles).

À noter que dans le cas (là aussi) particulier du rêve dans le rêve, le rêveur
est simplement renvoyé dans le rêve précédent. Ce phénomène est d'ailleurs à
l'origine d'un cas rare de rêve particulièrement traumatisant où un fiélon
poursuit un rêveur de rêve en rêve. Bien qu'il ne puisse atteindre Gaïa par ce
biais, ce cas rare est à l'origine de nombres de croyances supposant l'irruption
de fiélons en plein milieu de la nuit, au réveil de la victime.

### Intervention de vivants dans la Mer des Songes

Bien qu'il s'agisse là d'un cas de figure marginal, les conséquences de
l'intervention d'un vivant dans la Mer des Songes et sur le rêve d'un rêveur est
en tout point identique aux autres cas de figures, simplement que le vivant est
pleinement conscient et contrôle la situation.

Pour un vivant, la Mer des Songes se présente comme une vaste étendue d'eau dont
la surface est masquée par brouillard blanc laiteux ou gris, qui semble se
confondre avec le ciel. L'eau de la Mer des Songes est très bizarre : elle ne
désaltère pas et ne mouille pas (ou du moins sèche presque instantanément). La
Mer des Songes est peu profonde (l'eau arrive jusqu'aux mollets environ). La
surface en dessous semble tendre au toucher, mais à l'épreuve de tout choc.

Du point de vue du vivant, le monde du rêveur se matérialise par une zone
plongée dans un brouillard nettement plus épais et opaque, à l'intérieur duquel
tout est masqué. Il peut alors pénétrer dedans et, petit à petit, voire le monde
du rêveur se révéler à lui. Ce monde est plus ou moins élaboré selon la nature
du rêve et selon le rêveur. Les éléments composants le rêve sont plus ou moins
dense et compact, et le rêveur lui-même peut apparaître comme une sorte
d'illusion ou au contraire bien net, avec une apparence généralement très proche
de l'apparence réelle de l'individu.

**TODO** : ruines de l'ancienne civilisation des pères des dieux ?

### Après le 6e cycle

Après l'explosion de l'univers en une multitude de plans, l'accès à la Mer des
Songes est fermée aux non-morts. Cependant, la projection de l'âme du rêveur
dispose toujours d'un accès à la Mer des Songes, mais le parcours peut s'avérer
éprouvant à cause de l'Anti-mana qui en garde l'accès. C'est l'une des raisons
pour lesquels certaines nuits sont sans rêve ...

Aptitudes liées au mana
-----------------------

### Eau

Lié à la magie.

* Mémoire de l'Eau
* Écran magique

### Air

Lié à l'agilité.

* Déplacement rapide
* Téléportation

### Terre

Lié à l'endurance.

* Barrière de terre
* Tremblements de terre
* Armure renforcée

### Feu

Lié à la force.

* Boule de Feu
* Lame embrasée

### Esprit

Lié à la technique.

* Télékinésie
* Barrière mentale

### Ordre

* Soins
* Forgé du néant

### Chaos

* Destruction
* Dématérialisation

Races
-----

Format : nom / cycle d'apparition

### Douées de libre arbitre

Qui pourraient être jouées.

* Moraves / 2e
* Norsks / 2e
* Skands / 2e (Khans au 3e)
* Mohres / 2e
* Junins / 2e
* Ghils / 2e
* Elfes / 3e
* Nains / 3e
* Orcs / 3e
* Gobelinoïdes / 3e
* Arjunis / 3e
* Nezumis / 3e
* Edwins / 3e
* Firens / 3e
* Minrhodiens / 4e (reconnus comme une race au 4e, mais on peut les considérer
comme tel depuis le milieu du 3e)
* Cyborgs / 5e
* Androids / 5e

### Autres races "évoluées"

* Tengus / 3e
* Troglodytes / 2e (gardes certains accès à la Mer des Songes depuis le monde de
Gaïa ; encore à développer)
* Trolls / 3e
* Ettins / 3e
* Faeries / 3e
* Célestes / 1er (même avant)
* Fiélons / 1er (idem)
* Devoïdes / 6e (créatures d'anti-mana ; à développer)

### Races mineures

* Griffons
* Guivres
* Dragons
* Golems
* Mutandis
* Efrits
* Anti-êtres (à développer)
* ...