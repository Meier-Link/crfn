contrôle du mana par l'industrie.
=================================

Sur ce cycle, la magie n'existe pas, et le mana est utilisé comme une source
d'énergie, comme le pétrole,... Seulement, sa puissance est certes
phénoménale, mais surtout incontrôlable.

Les croyances
-------------

Au début du second cycle, Gaïa décida de retirer ses esprits de la surface,
pour laisser toute la place aux nouveaux êtres qui commençaient à se développer
depuis l'arrivée du cœur de Mana.

Cependant, il est resté dans la conscience collective le souvenir de Gaïa,
déesse suprême, et des esprits habitants la nature.

À la fin du cycle, Freya perçut les avancées des mortels dans la maîtrise du
mana, et notamment l'impact sur l'environnement, elle renvoya donc quelques
esprits pour se maintenir au courant de l'activité des mortels. C'est de cette
façon que les ancêtres de Lucian von Ruthven vont avoir l'occasion de croiser
une première fois Freya.

Géographie
----------

Le monde est partagé en 4 continents :
* Le Westeärd 
* Les Terres Équatoriales 
* Le Tochitôyô
* Le ... (trouver un nom à consonance slave)

Entre le Tochitôyô et les Terres Équatoriales se trouve un océan aussi vaste que
le Westeärd. Il s'étend des tropiques jusqu'au cercle polaire. À l'Est du
Tochitôyô, l'océan le plus vaste le sépare des côtes Ouest du Westeärd et des
Terres Équatoriales.

Histoire 
--------

### Politique 

Une nation domine le Westeärd et toute la moitié Ouest des Terres Équatoriales.
Cette nation est elle-même dominée par trois sociétés qui se partagent le
monopole.

Une nation domine le (futur) désert de Cristal et le Nord-est des Terres
Équatoriales. Cette nation n'a toléré aucune entreprise dominante (ou plutôt :
elles sont nationalisées sous l'autorité du chef du gouvernement). Cependant,
ils ont conclus un accord avec l'une des sociétés de l'Ouest pour accroître son
influence.

Aucune nation ne domine le Tochitôyô qui est partagé entre une dizaine de pays
dont l'équilibre est maintenu par une méga entreprise dont la puissance la place
au même niveau que les états les plus puissants du monde. 

Le Sud-Est des Terres Équatoriales est une zone de guerre civile que les
puissances maintiennent dans cet état pour y mener leurs conflits (sous la forme
d'une guerre froide).

La tension va fortement grimper lorsque l'état mégalomane du (futur) désert de
Cristal va entrer en possession de la centrale de Mana la plus puissante du
monde. Pour éviter de focaliser toute la force de frappe de la planète et
provoquer la dissension chez l'un de ses rivaux, le chef de cet état va faire
une proposition alléchante à la compagnie la plus puissante de l'Ouest sous la
forme d'un accès privilégié à la puissance et à certains secrets de la nouvelle
centrale.

Le résultat sera une guerre d'une violence sans précédent essentiellement
centrée sur le (futur) désert de Cristal. L'Ouest des Terres Équatoriales sera
également le centre d'un soulèvement qui va prendre des allures de guerre civile
à cause de la dissension provoquée par le gouvernement du Nord.

À noter que les puissances du Nord et de l'Ouest étaient clairement les plus
puissantes, celle du Tochitôyô étant plus centrée sur elle-même, ce continent
cherchera surtout à avoir une force de dissuasion suffisante pour détourner
l'intérêt des deux autres puissances.

Remarque :
* Force de l'Ouest, capitalisme. Tendance mégalomane et extraverti, à vouloir
rassembler Gaïa entière sous leur bannière.
* Force du Nord, communisme (utiliser des références à la commune de Paris pour
le nommage) On peut rajouter des tendances xénophobes pour accentuer le caractère
de cet état et une haine quasi religieuse à l'égard des puissances de l'Ouest.
* Force de l'Est, Chine / Japon, nombreux et travailleurs, mais moins
expansionnistes que les Japonais de la seconde guerre, sans doute refroidis par
un ancien conflit et la situation mondiale (se documenter sur le Japon 2e moitié
du XXe siècle pour les refs.) On peut également rajouter une tendance raciste /
peur de l'étranger pour accentuer la tendance à se replier sur eux-mêmes des
habitants du Tochitôyô.

À l'échelle d'un individu lambda, la situation de guerre froide a été
progressivement atténuée dans les médias pour donner l'impression que la
situation n'est pas si critique que ça.
Cependant, les habitants de certaines régions frontalières sont conscients de la
situation. Suivant les régions, soit ils restent calmes et cherchent à aller
voir ailleurs, soit fomentent des révoltes (manifestations violentes, etc.),
provoquant des troubles supplémentaires.
Note: les parents de la famille Ruthven seraient plutôt issus des premiers.

### Conséquences de la guerre

Effet d'une explosion de très grande envergure ?

#### Cratère

Provoquée par l'explosion. Environ 10km de diamètre.
Il ne reste absolument plus rien dans cette zone.

L'explosion provoquera également un "petit" trou dans l'atmosphère, expliquant
(conjointement avec la hausse des températures, développée plus loin), les
importantes perturbations du climat du Désert de Cristal et des régions au Sud
de celui-ci (cf. les tempêtes acides des années noires)

#### Onde de choc

Se propage aussi loin que le permettent les reliefs (cf. soliton)
Dévaste tout ce qui se trouve en surface, et provoque des tremblements de terre.
Les tremblements de terre ne sont pas interceptés par les reliefs, mais
s'atténuent avec la distance.

Vers l'Ouest du Désert de Cristal se trouve une chaîne de montagne qui va
protéger tout le Westeärd (y compris par le Nord). Le Westeärd sera donc épargné
par l'onde de choc, mais  les tremblements de terre y seront particulièrement
violent. Du coup, tout le continent sera en ruine, et l'essentiel des anciennes
régions habitées se retrouveront sous les eaux quand la Mer Intérieure va se
former (ça pourrait en faire une zone où trouver un paquet d'artefacts, pour les
plongeurs audacieux).
Remarque : la Grande Mer Intérieure n'existait pas au second cycle car la jeune
chaîne de montagne à l'Ouest du Westeärd était beaucoup moins haute, et même en
dessous du niveau de l'eau au Sud-Ouest du continent. Du coup, il y avait juste
une zone marécageuse là où se trouvent maintenant les Terres de Feu.

À noter que à l'endroit où se trouvait la région d'origine de la première femme
de Lucian, les ruines d'une citée importante seront recouverte d'une abondante
forêt, la fameuse forêt du Lhesovick, où Lucian et Shatten installeront le QG de
la future organisation des Fils de l'Ombre (alors appelée "Maîtres de l'Ombre").
Et c'est d'ailleurs pour cette raison que Lucian viendra là.

Vers le Nord, l'onde de choc va provoquer la dislocation des congères de glace
éternelles du Pôle Nord, provoquant une hausse du niveau des océans (mais pas si
significative que ça : à peine quelques 10aines de centimètres, ou "quelques
pieds").

Au Nord de la région où évolueront led aïeux de Lucian se formera des marécages
(à cause de la hausse du niveau des eaux) qui seront gelés les 3/4 de l'année.

Vers le Sud, aucun rempart naturelle pour bloquer l'onde de choc qui va se
transformer en tsunami en arrivant sur l'océan Équatoriale. C'est en passant
qu'elle va provoquer un soulèvement des terres en bord du Désert de Cristal,
amplifié plus tard pour former les falaises de Cristal.
Le tsunami va se propager pour ruiner toute les côtes Ouest des Terres
Équatoriales. Vers le Sud-Ouest, l'onde va également ruiner la moitié Nord du
continent. Selon toute vraisemblance, ce n'est pas l'explosion en Désert de
Cristal qui va impacter le Sud-Ouest des Terres Équatoriales (cf. plus loin).

Vers le Sud-Est, le tsunami va se propager jusqu'au Tochitôyô pour ruiner tout
le Nord et l'Ouest du continent jusqu'en son centre, où se trouvent les mêmes
sommets qu'au 3e cycle (alors moins élevés).
Le Nord et l'Est du continent abritait les populations les plus importantes et
les plus avancées du continent. Malheureusement, tout a été engloutis par le
tsunami, et les terres de l'Est, à faible altitude, se sont effondrées sous les
eaux. Au Sud, les populations Ghils étaient maintenus dans une paix relative par
la nation qui dominait ce continent. Une fois celle-ci réduite au néant, c'est
la barbarie qui reprit le dessus. C'est l'une des rares régions de la planète
qui aurait pu tirer avantage de la situation (devenir dominante) si la mentalité
des populations locales ne l'avait pas empêchée (on peut même dire qu'ils ont
perdu le souvenir d'avant les années noires alors qu'ils auraient pu le garder).

Vers l'Est et le Sud-Est, on retombe sur les Terres Équatoriales et le Westeärd
du côté des côtes Ouest. La nature des tsunamis fait que sa puissance était
pratiquement intacte et a tout ravagé jusqu'à la chaîne de montagne la plus
proche. 
La région épargnée que Lucian va trouver lors de son tout premier voyage, l'à
été grâce à la géographie de l'enclave où elle se trouve. À noter qu'on peut
aussi utiliser l'explosion du réacteur de cette enclave pour compléter
l'annihilation des civilisations du 2e cycle.

La jeune chaîne de montagne qui descend le long de la côte Ouest du Westeärd,
puis des Terres Équatoriales, sera successivement frappée par de violents
tremblements de terre, puis par le tsunami, ce qui explique sont intense
activité volcanique au cours du 3e cycle. Ce remue-ménage va également accroître
la vitesse à laquelle la chaîne va se développer.

#### Hausse de température

Se propage nettement moins loin et s'atténue progressivement.

À l'épicentre de l'explosion, elle atteindra plusieurs dizaines de milliers de
degrés faisant fondre absolument tout. Déjà à la périphérie du cratère, la
température à l'instant de l'explosion est divisée par 10. Au final, les points
les plus éloignés du désert de Cristal n'auront subit que des températures
avoisinants les 100° pendant quelques dizaines de minutes (c'est déjà pas mal).
La température va ensuite progressivement baisser jusqu'à environ :
* 50° au dessus des normales au bout d'1h et seulement dans le Desert de Cristal,
* 40° au dessus des normales le lendemain et seulement dans le Desert de Cristal,
* 20° au dessus des normales au bout d'une semaine dans le désert et les régions
avoisinantes,
* 10° au dessus des normales au bout d'un mois dans toute les régions à moins de
1000km autour du désert.
* 5° au dessus des normales au bout d'un an, s'étendant progressivement à tout
le globe.

La température mettra près de 10 ans avant de revenir à la normale, mais les
perturbations climatiques s'étaleront sur plus d'un siècle.

Remarque :
* À la période où eu lieu l'explosion, les températures dans le futur Désert de
Cristal évoluaient entre -10°C au Nord, et 10°C au Sud. La hausse a été plus
forte au Nord qu'au Sud (la centrale étant décalée vers le Nord du continent).
* Il est évident que l'évolution des températures aura détruit énormément de
formes de vie.

Le mana au second cycle
-----------------------

Le mana est contenu sous la forme d'artéfacts conférent des capacités spéciales
aux objets auxquels ils sont liés, sous forme brute (extraite des artéfacts) ou
sous forme de minerai, d'où il peut également être extrait.

Le problème concerne la façon de stocker le mana brut, que seul le système de
piles de mana parviendra à résoudre (dans une certaine mesure).

Fin du Cycle industriel
-----------------------

### Les réacteurs de mana

Lse réacteurs de mana, mis au point quelques décennies plus tôt, on besoin du
mana extrait des artéfacts et minerais pour fonctionner. Cependant, les
réacteurs en ont besoin en des quantités telles que le stockage d'artéfacts et
de minerai pose problèmes. Malheureusement, le stockage de mana brut pose
problème, et il faut littéralement brûler le minerai directement dans le
réacteur pour pouvoir l'utiliser.

Un étrange phénomène s'observe également autour des réacteurs de mana.
L'orsqu'ils brûlent du minerai ou des artefacts, ils dispèrsent d'infimes
quantités de mana dans l'environnement autour de la centrale. Les quantités de
mana ainsi dispersées sont infimes en comparaison de ce qu'il y aura au 3è
cycle, Cependant, d'étonnantes mutations seront observées sur la faune et la
flore. Du point de vue du 3è cycle, ce sont justes des formes de vie qui
développent des aptitudes magiques, mais pour les contemporains du second
cycle, ces mutations seront perçues comme des anomalies.

Remarque : On peut imaginer que les scientifiques de la fin du second cycles
auront déjà commencé à faire quelques expérimentations dans le plus grand
secret, mais qui n'aboutirait qu'à la création de mutants instables.

Les réacteurs de mana présentent un tel intérêt stratégique, qu'ils font
l'objet d'une surveillance extrême, et tout les phénomènes liés à leur
exploitations seront mal connus.
Leur coût fait également que seuls de puissantes sociétés pourrons les
construire.

### Les piles de mana

La mise au point de piles de mana sera l'aboutissement de décennies de
recherches pour trouver une solution de stockage du mana brut, et sera l'enjeux
de pouvoir des grandes puissances de la fin du second cycle.

Au départ, l'évolution tient essentiellement dans la concentration du mana dans
les minerais (qui sera d'ailleurs le moyen utilisé pour créer des artefacts
artificiellement. À noter qu'en fin de second cycle, les scientifiques ont
encore du mal à contrôler le résultat sur les caractéristiques des artéfacts
produits). La limite de la concentration de mana dans des artéfacts sera vite
atteinte :  ceux-ci deviennent rapidement instables.

La mise au point des piles de mana découlera de la mise au point des champs
magnétiques (par un certain Karl Tesla), qui permettra de repousser le seuil
d'instabilité.

Remarque : les piles de mana sont tellement grosses, qu'il faudra un semi
remorque pour en transporter une seule. L'encombrement découle essentiellement
du niveau de maîtrise des champs magnétiques des débuts. Leur taille sera
progressivement réduite, mais restera quand même relativement massive.

Les piles de mana sont stables mais encore limitées. L'origine du conflit sera
la mise au point de réacteurs de mana presque stables. L'origine de l'incident
étant un abus sur leur utilisation.

Remarque : le mana est une source inépuisable d'énergie. En théorie, il
suffirait de brûler un artefact ou suffisamment de minerai pour initier le
processus de production d'énergie. Le réacteur pourrait ensuite fonctionner
indéfiniment. La raion pour laquelle il fallait constamment user du stock
venait des soucis de fuite de mana (avant la mise au point des piles de mana,
il n'existait aucun moyen réellement fiable pour contenir le mana, et lors de
la combustion, les champs magnétiques ne peuvent être utilisé efficacement).

Entre la mise au point des tout premiers réacteurs et les années noires, il
s'écoulera plusieurs siècles. L'évolution entre les premiers et derniers
réacteurs tiendra essentiellement dans :
* l'accroissement de leur puissance,
* la réduction des pertes (fuites) de mana,
* l'évolution des techniques de stockage du mana.

### Fin de la guerre

À l'origine du conflit se trouvent d'une part une situation de guerre froide
entre les plus grandes puissances, et d'autre part la mise au point d'un
réacteur de mana particulièrement puissant. Le dirigeant se trouvant derrière
la mise au point de ce réacteur développera un projet d'usage militaire des
performances de cette centrale, dans un projet "trop" secret aux yeux des
autres grandes puissances.

La guerre froide finira par exploser dans l'une des régions les plus riches et
développées du monde, celle qui sera le futur Désert de Cristal.

L'incident provoquera d'abord une hausse quasi instantanée de la température
de plusieurs centaines de milliers de degrés dans un rayon de quelques dizaines
de kilomètres. L'onde de choc balayera tout dans une rayon de près de mille
kilomètres. Le dérèglement de température se propagera ensuite dans les jours
qui suivent. Au fur et à mesure que ce dernier s'étalera, la hausse de
température sera de moins en moins significative. Il n'y a que la zone du
(futur) désert de Cristal qui sera réellement soumis à des témpératures
extrêmes (plusieurs dizaines de milliers de degré en son centre, quelques
centaines de degrés à ses limites.
La température suivra ensuite une baisse régulière (style 1/x) sur quelques
siècles jusqu'à un retour à la normal bien après la fin du 3e cycle.

Remarque l'incident aura pu causer un "trou" dans l'atmosphère, justifiant les
dérèglements météorologiques dans les régions avoisinantes.

La région qui deviendra le désert de cristal était celle où eu lieu le conflit,
et celle qui était la plus riche du monde, partagée entre les nations les plus
puissantes de l'époque.

L'explosion du réacteur de mana dispersa également sa matière première partout
dans l'atmosphère et se dispersera jusque dans l'organisme des êtres vivants,
leur conférant une maîtrise plus ou moins poussée de la magie.
Dans la zone du désert de cristal, toutefois, les doses de mana perçuent par
les organismes seront tellement élevées que certaines mutations physiques
pourront être observées. (C'est au cours des premiers siècles que les rumeurs
sur l'existence de guivres et autres créatures monstrueuses dans le désert de
Cristal).

À l'explosion du réacteur, la dispersion de mana sera perçue par Heimdall et
Fafnir. Le premier en avertira Bahamut, tandis que le second commencera à
porter son attention sur le monde de Gaïa.
