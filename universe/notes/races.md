Les races de l'univers
======================

Remarque : comme l'ensemble de l'univers sera abordé sous le point de vue des
mortels, les noms de races en usage seront ceux utilisés par ces derniers.

Les pères des dieux
-------------------

* Véritable nom : inconnu
* Période d'appartition : inconnue

Les mortels n'ont même pas la notion de l'existence des pères des dieux, et les
les dieux l'ignore également.

Les dieux
---------

* Véritable nom : Dugilos
* Cycle d'appartition : bien avant le Cycle 1

En réalité, nul ne connaît l'origine des dieux, pas même les dieux eux-mêmes.
Ils sont les bâtisseurs des mondes qui composent la galaxie.

Cependant, ils furent créés par une race qui s'était répandue pratiquement dans tout
l'univers pour le rebâtire, alors qu'eux-mêmes l'avait détruit.

Les esprits
-----------

* Véritable nom : Aimagus
* Cycle d'appartition : 1er cycle

Les mortels appellent les aimagus "esprit" à cause de leur immortalité et de
leur lien avec Dugilos, qu'ils considèrent comme des dieux.

En réalité, les Aimagus ont été créé par les Dugilos pour les assister dans
dans leurs tâches. Ils sont (normalement) dépourvu de lirbre arbitre et ne font
qu'exécuter les tâches que leur ont assigné les Aimagus.

Deux groupes d'esprits font toutefois exception à ces règles :
* Les Célestes, qui peuplent le Collysée et le Panthéons.
* Les fiélons, qui peuplent le Qlyphoth et l'Abyss.

Ces deux groupes sont considérés commes des esprits, mais sont d'origine
incertaines. On sait seulement qu'ils existent depuis au moins aussi longtemps
que les dieux (à développer).

Les mortels
-----------

* Véritable nom : les mortels
* Cycle d'appartition : 1er cycle

Les mortels sont une création originale de Gaïa qui a eu l'étrange idée de voir
comment évoluerait des êtres qui doivent se reproduirent pour compenser le fait
qu'ils aient une espérance de vie limitée.

Leur développement changera totalement à partir du moment où la magie
commencera à les influencer : en effet, les dieux ne peuvent pas d'eux-mêmes
leur conférer le libre arbitre, qu'ils ne développerons qu'à partir du 2e
cycle.

Les mortels sont répartis en plusieurs peuples avec leurs caractéristiques
propres.

Ici n'est fournit qu'une liste incomplète. Les peuples du monde de Gaïa seront
développés dans les cycles 2 et 3.

### Peuples apparus au 2e cycle

Pour des raisons de simplicité, nous utiliseront les noms des continents du
3è cycles (ceux du second cycles sont inconnus pour le moment).

* Les Mohres en terre équatoriales.
* Les Moraves en Westeärd.
* Les Norsks dans l'extrême Nord-Est du Westeärd.
* Les Junins en le Tochitoyo

### Peuples apparus au 3e cycle

Lorsque le Mana se diffusera sur l'ensemble du monde de Gaïa à la fin du 2e
cycle, de nombreux peuples feront leur apparition, issu de l'influence du mana
sur l'environnement en local.

* Les elfes, dans les forêts du Westeärd.
* Les nains, dans les montagnes au Nord du Westeärd.
* Les gobelins, dans les montagnes au Sud du Westeärd et des Terres
Équatoriales
* Les orques, dans les plaines de Westeärd et de Terres Équatoriales
* Les ettins, dans les marais qui deviendrons plus tard la Mer Intérieure du
Westeärd
* Les nezumis, dans les marais au Nord de Tochitoyo (à revoir).
* Les tengus, dans les hauteurs des montagnes de Tochitoyo.
* Les Edwins, issus de l'union de certains esprits de Gaïa et de mortels.
* Les firens, créés artificiellements à partir de fiélons et de mortels.

Les races crées par les mortels
-------------------------------

À partir du 5è cycle, les mortls commencent à créer eux-mêmes des formes de
vies de plus en plus évoluées, certaines totalement artificiels, d'autres en se
manipulant eux-mêmes.

Ces formes de vies seront de plus en plus développées, jusqu'à ce qu'elles
puissent êtres considérées comme des races à part entière, avec leurs
spécificités.
